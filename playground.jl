using Random
using Plots
using Zygote

# gr()
include("src/draw.jl")
include("src/build_tree.jl")
include("visualize.jl")

x_1 = [1.0, 1.0]
x_2 = [-1.0, -0.5]
s_2 = reshape([1.0, -0.45, -0.45, 0.5], (2, 2))

function likelihood(x::Vector{Float64})
    n1 = exp(-sum((x - x_1) .^ 2))
    tmp_1 = transpose(x - x_2) * inv(s_2)
    n2 = exp(-sum(reshape(tmp_1, (1, 2)) * reshape(x - x_2, (2, 1))))
    return log(n1 + n2)
end

epsilon = 0.03

theta_0 = [0.0, 0]
momentum_0 = randn(2)
hamiltonian_ratio = rand() * exp(hamiltonian(likelihood, theta_0, momentum_0))

tree, paths = build_tree(theta_0, momentum_0, hamiltonian_ratio, true, 3, epsilon, likelihood)

path = res[8]


theta_res, increased, paths = draw_one([1.0, 1.0], epsilon, likelihood, true);
theta_res
increased

# Plot the draw.
plot_path(theta_0, increased, paths, likelihood)

# Plot the draws.
thetas, starts, increased, paths = draw_n(5, theta_0, epsilon, likelihood);

plot_draws(starts, increased, paths, likelihood, 4)
plot_path(starts[2], increased[2], paths[2], likelihood, 4)
