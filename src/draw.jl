using Random

include("utils.jl")
include("build_tree.jl")


function draw_one(θ::Vector{Float64}, ϵ, likelihood, return_paths::Bool = false)
    increase_collected = Bool[]
    path_collected = Matrix{Float64}[]
    # Draw a random initial momentum.
    r = randn(length(θ))
    hamiltonian_ratio = rand() * exp(hamiltonian(likelihood, θ, r))
    # Initialize the values
    tree = Tree(θ, r, θ, r, θ, 1, true)
    num_steps = 1
    while tree.s
        increases = rand() > 0.5
        if increases
            θ_start = tree.θ_upper
            r_start = tree.r_upper
        else
            θ_start = tree.θ_lower
            r_start = tree.r_lower
        end
        new_tree, paths = build_tree(θ_start, r_start, hamiltonian_ratio, increases, num_steps, ϵ, likelihood)
        append!(increase_collected, increases)
        append!(path_collected, [paths])
        # Apply the new tree borders.
        if increases
            tree.θ_upper = new_tree.θ_upper
            tree.r_upper = new_tree.r_upper
        else
            tree.θ_lower = new_tree.θ_lower
            tree.r_lower = new_tree.r_lower
        end
        # Stochastically select the new theta.
        if rand() <= new_tree.n / (tree.n + new_tree.n)
            tree.θ = new_tree.θ
        end
        # Overwrite the running values.
        tree.n = tree.n + new_tree.n
        u_turn_lower = sum((tree.θ_upper - tree.θ_lower) .* tree.r_lower) >= 0
        u_turn_upper = sum((tree.θ_upper - tree.θ_lower) .* tree.r_upper) >= 0
        tree.s = new_tree.s & u_turn_lower & u_turn_upper
        # Increase the number of steps taken.
        num_steps = num_steps + 1
    end
    if return_paths
        return tree.θ, increase_collected, path_collected
    else
        return tree.θ
    end
end

function draw_n(n, θ_0, ϵ, likelihood)
    # Instantiate the arrays for the collected results.
    starts = Array{Vector{Float64}, 1}()
    increased = Array{Vector{Bool}, 1}()
    paths = Array{Array{Matrix{Float64}, 1}, 1}()

    θ = zeros(n + 1, length(θ_0))
    θ[1, :] = θ_0
    for i in 1:n
        θ[i + 1, :], increase, path = draw_one(θ[i, :], ϵ, likelihood, true)
        append!(starts, [θ[i, :]])
        append!(increased, [increase])
        append!(paths, [path])
    end
    return θ[2:n+1, :], starts, increased, paths
end

