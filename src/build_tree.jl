
DELTA_MAX = 100.0

include("utils.jl")


function leapfrog_step(θ, momentum, epsilon, likelihood)
    momentum = momentum .+ epsilon / 2 * likelihood'(θ)
    θ = θ .+ epsilon * momentum
    momentum = momentum .+ epsilon / 2 * likelihood'(θ)
    return θ, momentum
end


function build_tree(
    θ::Vector{Float64},
    r::Vector{Float64},
    hamiltonian_ratio::Float64,
    increase::Bool,
    num_steps::Int64,
    epsilon::Float64,
    likelihood,
)
    if num_steps == 0
        # Build the final leaf.
        if increase
            epsilon_step = epsilon
        else
            epsilon_step = -epsilon
        end
        θ_new, r_new = leapfrog_step(θ, r, epsilon_step, likelihood)
        # Evaluate the hamiltonian and check the stopping conditions.
        hamil = hamiltonian(likelihood, θ_new, r_new)
        n = Int(hamiltonian_ratio <= exp(hamil))
        s = hamil > (log(hamiltonian_ratio) - DELTA_MAX)
        return Tree(θ_new, r_new, θ_new, r_new, θ_new, n, s), reshape(θ_new, (1, length(θ)))
    end
    # Build the two subtrees.
    tree, path = build_tree(
        θ,
        r,
        hamiltonian_ratio,
        increase,
        num_steps - 1,
        epsilon,
        likelihood,
    )
    if tree.s
        # If this is not the case, there is no need to even build the second tree, since no θ from this tree will be sampled.
        # Pick the start point based on the direction picked.
        if increase
            θ_start = tree.θ_upper
            r_start = tree.r_upper
        else
            θ_start = tree.θ_lower
            r_start = tree.r_lower
        end
        tree_2, path_2 = build_tree(
            θ_start,
            r_start,
            hamiltonian_ratio,
            increase,
            num_steps - 1,
            epsilon,
            likelihood,
        )
        # Pick the theta and r again based on the direction.
        if increase
            tree.θ_upper = tree_2.θ_upper
            tree.r_upper = tree_2.r_upper
            path = vcat(path, path_2)
        else
            tree.θ_lower = tree_2.θ_lower
            tree.r_lower = tree_2.r_lower
            path = vcat(path_2, path)
        end
        # Sample a theta, compute n and s.
        if rand() < tree.n / (tree.n + tree_2.n)
            # Choose the second θ.
            tree.θ = tree_2.θ
        end
        u_lower = sum((tree.θ_upper - tree.θ_lower) .* tree.r_lower) >= 0
        u_upper = sum((tree.θ_upper - tree.θ_lower) .* tree.r_upper) >= 0
        tree.s = tree_2.s & u_lower & u_upper
        tree.n = tree.n + tree_2.n
    end
    return tree, path
end


