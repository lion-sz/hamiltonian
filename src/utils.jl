
field_names = (:θ_lower, :r_lower, :θ_upper, :r_upper, :θ, :n, :s)
tree_tuple = NamedTuple{field_names}


mutable struct Tree
    θ_lower::Vector{Float64}
    r_lower::Vector{Float64}
    θ_upper::Vector{Float64}
    r_upper::Vector{Float64}
    θ::Vector{Float64}
    n::Int64
    s::Bool
end



function hamiltonian(likelihood, θ::Vector{Float64}, momentum::Vector{Float64})
    likelihood(θ) - 1 / 2 * sum(momentum .^ 2)
end

