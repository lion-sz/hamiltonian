
function plot_path(start, increased, paths, likelihood, axis_lim=3, n_ticks=100)
    # First create the axis grid.
    x_ticks = range(-axis_lim, axis_lim, length=n_ticks)
    y_ticks = range(-axis_lim, axis_lim, length=n_ticks)
    contour_func = (x, y) -> exp(likelihood([x, y]))
    # Compute the number of observations for each draw.
    n_obs = map(x -> size(x, 1), paths)
    start_x = start[1]
    start_y = start[2]
    # Create the animation.
    anim = Animation()
    # The first index contains the contour, the second the overall start plot and the third one the series start points.
    plt = contour(x_ticks, y_ticks, contour_func, xlim=(-axis_lim, axis_lim), ylim=(-axis_lim, axis_lim), title="NUTS Draw", legend=false, marker=10)
    scatter!([start_x], [start_y], color="black")
    scatter!([], [], color="red")
    # Go through each build tree.
    for i in eachindex(n_obs)
        path = paths[i]
        # if !increased[i]
        #     path = reverse(path)
        # end
        tree_start_x = path[1, 1]
        tree_start_y = path[1, 2]
        # Add the start point.
        push!(plt, 3, tree_start_x, tree_start_y)
        # Initialize the line.
        plt = plot!([], [], color="black")
        # Go through each observation and add it to the line.
        for j in 1:n_obs[i]
            # Draw the contour and start points
            push!(plt, 3 + i, path[j, 1], path[j, 2])
            frame(anim)
        end
        # pause for 2 frames
        for j in 1:2
            frame(anim)
        end
    end
    gif(anim, "test.gif", fps=8)
end


function plot_draws(starts, increased, paths, likelihood, axis_lim=3, n_ticks=100)
    # First create the axis grid.
    x_ticks = range(-axis_lim, axis_lim, length=n_ticks)
    y_ticks = range(-axis_lim, axis_lim, length=n_ticks)
    contour_func = (x, y) -> exp(likelihood([x, y]))
    # Create the animation.
    anim = Animation()
    # Go through the draws.
    for draw_nr in eachindex(starts)
        draw_start = starts[draw_nr]
        draw_increased = increased[draw_nr]
        draw_paths = paths[draw_nr]
        # Compute the number of observations for each tree.
        n_obs = map(x -> size(x, 1), draw_paths)
        # Add the contour and empty scatter plots.
        # The first index contains the contour, the second the overall start plot and the third one the series start points
        plt = contour(x_ticks, y_ticks, contour_func, xlim=(-axis_lim, axis_lim), ylim=(-axis_lim, axis_lim), title="NUTS Draw", legend=false, marker=10)
        scatter!([draw_start[1]], [draw_start[2]], color="black")
        scatter!([], [], color="red")
        # Go through each build tree.
        for i in eachindex(n_obs)
            path = draw_paths[i]
            # if !draw_increased[i]
            #     path = reverse(path)
            # end
            tree_start_x = path[1, 1]
            tree_start_y = path[1, 2]
            # Add the start point.
            push!(plt, 3, tree_start_x, tree_start_y)
            # Initialize the line.
            plt = plot!([], [], color="black")
            # Go through each observation and add it to the line.
            for j in 1:n_obs[i]
                # Draw the contour and start points
                push!(plt, 3 + i, path[j, 1], path[j, 2])
                frame(anim)
            end
            # pause for 2 frames
            for j in 1:2
                frame(anim)
            end
        end
    end
    gif(anim, "test.gif", fps=8)
end
